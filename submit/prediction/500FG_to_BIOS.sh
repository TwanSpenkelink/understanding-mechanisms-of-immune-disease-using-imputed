#!/bin/bash
#SBATCH --job-name=Imputation
#SBATCH --output=slurm_out/prediction_%j.out
#SBATCH --error=slurm_out/prediction_%j.err
#SBATCH --time=05:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=30gb
#SBATCH --nodes=1
#SBATCH --open-mode=append
#SBATCH --export=NONE
#SBATCH --get-user-env=L

echo "----------------------------------------------------"
echo "[INFO] Starting imputation"
echo "----------------------------------------------------"

module load R/3.3.3-foss-2015b
module load binutils

Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/PredictV2.R \
-g /groups/umcg-bios/tmp04/users/umcg-tspenkelink/imputation/data/LL_dosages_500FG_harmonized_cqtl_5e-04.txt \
-t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/cytokines/cytokines_imputation_info.txt \
-m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_no_alpha_zero/cytokines \
-o /groups/umcg-bios/tmp04/users/umcg-tspenkelink/imputation/result \
-n 10 \
-k 10 \
-p 5e-04
