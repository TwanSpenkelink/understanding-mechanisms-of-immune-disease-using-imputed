library(data.table)
file.names <- Sys.glob("/groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/cellcounts_summary_stat/*.Rdata")

#file.names <- Sys.glob("/home/twan/calculon/imputation/data/500FG/cellcounts_summary_stat/IT196_merged_ccQTLS.Rdata")


index <- 1
data <- list()

for (file.name in file.names) {
  cat(paste0("[INFO]\tReading file:",file.name,"\n"))
  load(file.name)
  data[[index]] <- merged_ccQTLS_complete[,c(5, 6, 7, 8, 9, 10)]
  index <- index + 1
}

data <- rbindlist(data)
#data <- data[c(5, 6, 7, 8, 9, 10)]

write.table(data, "/groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/cellcounts_summary_stat/cellcounts_summary_stat.txt", quote = 
              FALSE, row.names = FALSE, sep = "\t")