#!/bin/bash
#SBATCH --job-name=Imputation
#SBATCH --output=slurm_out/imputation_%j.out
#SBATCH --error=slurm_out/imputation_%j.err
#SBATCH --time=02:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=30gb
#SBATCH --nodes=1
#SBATCH --open-mode=append
#SBATCH --export=NONE
#SBATCH --get-user-env=L

echo "----------------------------------------------------"
echo "[INFO] Starting imputation"
echo "[INFO] Trait: $1: $2"
echo "[INFO] QTL p-value: $3"
echo "----------------------------------------------------"

module load R
module load binutils
Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/Imputation.R \
-g /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG_to_200FG_dosage_500FG_samples.txt \
-i /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_data.txt \
-q /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_summary_stat.txt \
-o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_500FG_to_200FG/"$1" \
-t "$2" \
-p "$3" \
-n 1 \
-k 10

echo "----------------------------------------------------"
echo "[INFO] Imputation done"
echo "----------------------------------------------------"
