#!bin/bash

for VAR in cytokines cellproportions cellcounts hormones immunoglobulins mediators platelets

do

  echo "----------------------------------------------------"
  echo "[INFO] Predicting $VAR"
  echo "----------------------------------------------------"

  sbatch 500FG_to_BIOS.sh "$VAR"

done
