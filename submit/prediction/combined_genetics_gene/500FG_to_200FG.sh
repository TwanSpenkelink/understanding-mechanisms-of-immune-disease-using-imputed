#!/bin/bash
#SBATCH --job-name=Imputation
#SBATCH --output=slurm_out/prediction_%j.out
#SBATCH --error=slurm_out/prediction_%j.err
#SBATCH --time=05:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=30gb
#SBATCH --nodes=1
#SBATCH --open-mode=append
#SBATCH --export=NONE
#SBATCH --get-user-env=L

echo "----------------------------------------------------"
echo "[INFO] Starting imputation"
echo "----------------------------------------------------"

module load R/3.3.3-foss-2015b
module load binutils

Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/Predict.R \
-g /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG_to_200FG_dosage_200FG_samples.txt \
-t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/cytokines/cytokines_imputation_info.txt \
-m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_no_alpha_zero/cytokines \
-o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/500FG_to_200FG_prediction/ \
-n 10 \
-k 10 \
-p 5e-04
