#!bin/bash

module load R/3.3.3-foss-2015b

for VAR in cytokines cellproportions cellcounts hormones immunoglobulins mediators metabolites platelets
#for VAR in cytokines

do

  echo "----------------------------------------------------"
  echo "[INFO] Creating plots for $VAR"
  echo "----------------------------------------------------"

  Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/EvaluateModel.R \
  -t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$VAR"/"$VAR"_imputation_info.txt \
  -m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_gene_expression_no_alpha_zero/"$VAR" \
  -n 10 \
  -k 10 \
  -c "e" \
  -o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_gene_expression_no_alpha_zero/

done
