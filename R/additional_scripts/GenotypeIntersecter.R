library(data.table)

#phenotype.list <- c("cytokines", "platelets", "mediators","hormones","cellcounts","cellproportions","metabolites","immunoglobulins")
phenotype.list <- c("cellproportions")

ll.snps <- fread("gunzip -c /groups/umcg-bios/tmp04/users/umcg-obbakker/data/LL/dosage_matrix/500FG_harmonized/merged/merged_dosages.txt.gz", header = TRUE, data.table=FALSE)
rownames(ll.snps) <- ll.snps$id
ll.snps$id <- NULL

for (phenotype in phenotype.list){
  qtl <- fread(paste0("/groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/",phenotype,"/",phenotype,"_summary_stat.txt"), header=TRUE)
  qtl <- subset(qtl, pvalue < 5e-4)
  int <- intersect(rownames(ll.snps), qtl$snps)
  ll.snps.qtl <- ll.snps[int,]
  
  print(head(ll.snps.qtl))
  
  ll.snps.qtl <- cbind(id = rownames(ll.snps.qtl), ll.snps.qtl)
  fwrite(ll.snps.qtl,paste0("/groups/umcg-bios/tmp04/users/umcg-tspenkelink/imputation/data/imputation_database/",phenotype,"/",phenotype,"_qtl_5e-4_dosage.txt"), quote=FALSE, sep= " ", row.names = TRUE, col.names = TRUE)
  
}


