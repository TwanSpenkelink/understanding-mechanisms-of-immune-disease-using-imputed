#!/bin/bash
#SBATCH --job-name=Prediction
#SBATCH --output=slurm_out/prediction_%j.out
#SBATCH --error=slurm_out/prediction_%j.err
#SBATCH --time=03:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=40gb
#SBATCH --nodes=1
#SBATCH --open-mode=append
#SBATCH --export=NONE
#SBATCH --get-user-env=L

echo "----------------------------------------------------"
echo "[INFO] Starting imputation"
echo "----------------------------------------------------"

module load R/3.3.3-foss-2015b
module load binutils

Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/Predict.R \
-g /groups/umcg-bios/tmp04/users/umcg-tspenkelink/imputation/data/imputation_database/"$1"/"$1"_qtl_5e-4_dosage.txt \
-e /groups/umcg-bios/tmp04/users/umcg-tspenkelink/imputation/data/imputation_database/gene_expression/gene_expression.txt \
-t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_imputation_info.txt \
-m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/"$1" \
-o /groups/umcg-bios/tmp04/users/umcg-tspenkelink/imputation/result/prediction_combined_genetics_gene \
-n 10 \
-k 10 




