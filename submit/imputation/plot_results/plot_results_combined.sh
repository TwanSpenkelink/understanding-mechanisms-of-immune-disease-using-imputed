#!bin/bash

module load R/3.3.3-foss-2015b

for VAR in cytokines cellproportions cellcounts hormones immunoglobulins mediators metabolites platelets
#for VAR in cellproportions



do
 for THR in 5e-4 5e-5 5e-6

 do
    echo "----------------------------------------------------"
    echo "[INFO] combined genetics: $VAR"
    echo "----------------------------------------------------"

    Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/EvaluateModel.R \
    -t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$VAR"/"$VAR"_imputation_info.txt \
    -m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/"$VAR" \
    -n 10 \
    -k 10 \
    -c "c" \
    -p "$THR" \
    -o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/

    echo "----------------------------------------------------"
    echo "[INFO] Combined genetics + gene expression: $VAR | $THR"
    echo "----------------------------------------------------"
done
done
