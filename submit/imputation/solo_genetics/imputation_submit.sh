#!/bin/bash
#SBATCH --job-name=Imputation
#SBATCH --output=slurm_out/imputation_%j.out
#SBATCH --error=slurm_out/imputation_%j.err
#SBATCH --time=3-00:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=200gb
#SBATCH --nodes=1
#SBATCH --open-mode=append
#SBATCH --export=NONE
#SBATCH --get-user-env=L

echo "----------------------------------------------------"
echo "[INFO] Starting imputation"
echo "[INFO] Trait: $1: $2"
echo "----------------------------------------------------"

module load R/3.3.3-foss-2015b
module load binutils
Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/Imputation.R \
-g /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/genetics/all_snp_dosages.txt \
-i /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_data.txt \
-q /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_summary_stat.txt \
-o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_no_alpha_zero_no_qtl/"$1" \
-t "$2" \
-n 1 \
-k 10

echo "----------------------------------------------------"
echo "[INFO] Imputation done"
echo "----------------------------------------------------"
