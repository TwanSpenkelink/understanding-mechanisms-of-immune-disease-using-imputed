#!bin/bash

module load R/3.3.3-foss-2015b

#for VAR in cytokines cellproportions cellcounts hormones immunoglobulins mediators metabolites platelets
for VAR in cellproportions



do

  echo "----------------------------------------------------"
  echo "[INFO] Creating plots for $VAR"
  echo "----------------------------------------------------"

  for THR in 5e-04 5e-05 5e-06 5e-07
  do

    echo "----------------------------------------------------"
    echo "[INFO] Solo genetics: $VAR | $THR"
    echo "----------------------------------------------------"

    Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/EvaluateModel.R \
    -t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$VAR"/"$VAR"_imputation_info.txt \
    -m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_no_alpha_zero/"$VAR" \
    -n 10 \
    -k 10 \
    -p "$THR" \
    -o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_no_alpha_zero/

    echo "----------------------------------------------------"
    echo "[INFO] Combined genetics + gene expression: $VAR | $THR"
    echo "----------------------------------------------------"

    Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/EvaluateModel.R \
    -t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$VAR"/"$VAR"_imputation_info.txt \
    -m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/"$VAR" \
    -n 10 \
    -k 10 \
    -p "$THR" \
    -o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/ \
    -c

    echo "----------------------------------------------------"
    echo "[INFO] Compare combined and solo models for: $VAR | $THR"
    echo "----------------------------------------------------"

    Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/ScatterPlot.R \
    -t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$VAR"/"$VAR"_available.txt \
    -c /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/"$VAR" \
    -s /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_no_alpha_zero/"$VAR" \
    -n 10 \
    -k 10 \
    -p "$THR" \
    -o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/

  done
done
