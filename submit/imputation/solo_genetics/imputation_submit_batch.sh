#!/bin/bash

echo "--------------------------------------------------"
echo "[INFO]        Imputation of immune traits"
echo "--------------------------------------------------"

file="/groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/$1/$1_available.txt"
while IFS= read -r line
do
  echo "-------------------------------------------------------------"
  echo "[INFO] Starting imputation for $line"
  echo "-------------------------------------------------------------"

  sbatch imputation_submit.sh "$1" "$line"
done < "$file"

