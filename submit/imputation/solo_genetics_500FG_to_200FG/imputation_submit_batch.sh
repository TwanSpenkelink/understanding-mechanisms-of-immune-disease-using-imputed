#!/bin/bash

echo "--------------------------------------------------"
echo "[INFO]        Imputation of immune traits"
echo "--------------------------------------------------"

#!/bin/ bash
file="/groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/$1/$1_available.txt"
while IFS= read -r line
do

  for THR in 5e-04; do

    echo "-------------------------------------------------------------"
    echo "[INFO] Starting imputation for $line and threshold $THR"
    echo "-------------------------------------------------------------"

    sbatch imputation_submit.sh "$1" "$line" "$THR"
  done
done < "$file"

