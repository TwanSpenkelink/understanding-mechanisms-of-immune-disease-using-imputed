#!/bin/bash
#SBATCH --job-name=Imputation
#SBATCH --output=slurm_out/imputation_%j.out
#SBATCH --error=slurm_out/imputation_%j.err
#SBATCH --time=02:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=30gb
#SBATCH --nodes=1
#SBATCH --open-mode=append
#SBATCH --export=NONE
#SBATCH --get-user-env=L

echo "----------------------------------------------------"
echo "[INFO] Starting imputation"
echo "[INFO] Trait: $1: $2"
echo "----------------------------------------------------"

module load R/3.3.3-foss-2015b
module load binutils
Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/Imputation.R \
-g /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/genetics/all_snp_dosages.txt \
-i /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_data.txt \
-q /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_summary_stat.txt \
-c /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_correlations.txt \
-o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/combined_genetics_gene_no_alpha_zero/"$1" \
-e /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/gene_expression/500FG_RNAseq_TMM_normalized_read_counts.txt \
-t "$2" \
-n 10 \
-k 10

echo "----------------------------------------------------"
echo "[INFO] Imputation done"
echo "----------------------------------------------------"
