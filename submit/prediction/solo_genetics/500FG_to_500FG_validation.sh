#!/bin/bash
#SBATCH --job-name=Prediction
#SBATCH --output=slurm_out/prediction_%j.out
#SBATCH --error=slurm_out/prediction_%j.err
#SBATCH --time=04:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=100gb
#SBATCH --nodes=1
#SBATCH --open-mode=append
#SBATCH --export=NONE
#SBATCH --get-user-env=L

echo "----------------------------------------------------"
echo "[INFO] Starting imputation"
echo "----------------------------------------------------"

module load R/3.3.3-foss-2015b
module load binutils

Rscript /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/scripts/R/Predict.R \
-g /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/genetics/cytokines_snp_dosages.txt \
-t /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/data/500FG/imputation_database/"$1"/"$1"_imputation_info.txt \
-m /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/solo_genetics_no_alpha_zero/"$1" \
-o /groups/umcg-wijmenga/tmp04/umcg-tspenkelink/imputation/output/500FG_prediction_solo_genetics_test/ \
-n 10 \
-k 10
